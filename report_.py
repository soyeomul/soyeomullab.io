#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 메일건 CURL 메일전송 예제 코드

import sys
import os
from subprocess import Popen, PIPE, call

import lex__ # custom module



### 전자메일 주요 환경 변수 (사용자 정의)
#
# MAIL_FROM: 보내는 이 주소
# RCPT_TO: 받는이 주소
# MAIL_API_KEY: 보내는이 (메일건) API 키
# MAIL_API_ADDR: 보내는이 (메일건) API 주소

MAIL_DOMAIN = os.getenv('THANKS_FQDN')
MAIL_USER = os.getenv('THANKS_GOPHER')
MAIL_NAME = os.getenv('THANKS_DOMOONSOJA')
MAIL_FROM = "%s <%s@%s>" % (
    MAIL_NAME,
    MAIL_USER,
    MAIL_DOMAIN,
)
RCPT_TO = os.getenv('GITLAB_USER_EMAIL')
MAIL_API_KEY = lex__.hbh(os.getenv('THANKS_MGK'))
MAIL_API_ADDR = "https://api.mailgun.net/v3/%s/messages" % (
    MAIL_DOMAIN,
)
#
### 전자메일 주요 환경 변수 (사용자 정의) 



### 기타 변수 ###
#
def asdf():
    cmd_report = "hostname; echo; ls -l /var/log"
    cmd_time = "LC_TIME=C.UTF-8 date"
    
    _try1 = Popen(cmd_report, stdout=PIPE, shell=True)
    _try2 = Popen(cmd_time, stdout=PIPE, shell=True)
    _output1 = _try1.communicate()[0].decode("utf-8").strip()
    _output2 = _try2.communicate()[0].decode("utf-8").strip()

    return _output1, _output2


def PT():
    PT = os.getenv('CI_PROJECT_TITLE')
    if PT == None:
        return "^^^"

    return PT

    
MAIL_SUBJECT = "깃랩 보고서 (%s): %s" % (PT(), asdf()[1])

MAIL_TEXT = """
항상 언제나 테스트 메일입니다 
curl 메일건 api 로 보냅니다 

아름다운 우리나라
금수강산 대한민국

ㅎㅎㅎ 

^고맙습니다 _布德天下_ 감사합니다_^))//

%s
""" % (asdf()[0])
#
### 기타 변수 ###



curl_mailgun = """\
curl -s --user 'api:%s' %s \
-F from='%s' \
-F to=%s \
-F subject='%s' \
-F text='%s'""" % (
    MAIL_API_KEY,
    MAIL_API_ADDR,
    MAIL_FROM,
    RCPT_TO,
    MAIL_SUBJECT,
    MAIL_TEXT,
)



def send():
    call(curl_mailgun, shell=True)

    return 0


def _print():
    print(curl_mailgun)

    return 0



if __name__ == "__main__":
    if sys.argv[1] == "--send":
        sys.exit(send())
    if sys.argv[1] == "--print":
        sys.exit(_print())
    
# 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
# 마지막 갱신: 2021년 10월 14일
