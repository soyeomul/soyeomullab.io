# -*- coding: utf-8 -*-

import sys
import os
import json
from datetime import datetime
from platform import node

#import distro # pip3 module

import floss # custom module
import soyeomul # custom module
import _uname # custom module
  
# CREDIT BEGINS HERE
# 깃랩 서버의 환경 정보를 출력합니다
_GEN = "생성도구: " + "Python " + sys.version
_OSE = "생성도구 운영체제: %s (%s)" % (
    _uname.distro(),
    _uname.uname(),
)

_OSN = "생성도구 콤푸타 이름: " + node()
_LUP = "마지막 갱신: " + str(datetime.utcnow()) + " (UTC)"
_CON = "연락처: %s" % (os.getenv('CI_COMMIT_AUTHOR'))
_SIG = "^고맙습니다 _布德天下_ 감사합니다_^))//"

CREDIT = [_GEN, _OSE, _OSN, _LUP, _CON, _SIG,]
CREDIT = dict([tuple(x) for x in enumerate(CREDIT, start=0)])
# CREDIT ENDS HERE

KEY = \
    ["FLOSS_ORG"] + \
    ["FLOSS_PROJ"] + \
    ["gitlab.com/soyeomul"] + \
    ["CREDIT"]

VALUE = \
    [floss.ORG] + \
    [floss.PROJ] + \
    [soyeomul.REPO] + \
    [CREDIT]

dd = dict(zip(KEY, VALUE))
dj = json.dumps(dd, indent=8, ensure_ascii=False)

dhtml = """\
<!DOCTYPE html>
<!-- -*- coding: %s -*- -->
<!-- 편집: GNU Emacs 27.1 (Ubuntu 18.04) -->
<!-- 참고문헌(HTML5): https://www.w3.org/TR/html52/ -->

<html lang="ko">
<head>
  <meta charset="%s" />
  <title>%s</title>
  <meta name="Genetator" content="%s" />
  <meta name="Modified" content="%s" />
  <style>
    body {
      background-image: url("%s");
      height: %s;
      background-position: center;
      background-repeat: no-repeat;
      background-size: cover;      
    }
  </style>
</head>
<body>
<pre>
%s
</pre>
</body>
</html>""" % (
    "utf-8",
    "utf-8",
    "The World is Linux and OpenSource ^^^",
    _GEN[6:],
    _LUP[8:],
    "three_emperor_penguins.png",
    "100%",
    dj,
)

# 끝으로 화면에 뿌립니다... 소심하게 살포시...
if __name__ == "__main__":
    print(dhtml)

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2021년 10월 15일
