# -*- coding: utf-8 -*-

from subprocess import Popen, PIPE


def uname():
    cmd = "uname -srm"

    _try = Popen(cmd, stdout=PIPE, shell=True)
    _output = _try.communicate()[0].decode("utf-8").strip()

    return _output

def distro():
    cmd = "lsb_release -d -s"

    _try = Popen(cmd, stdout=PIPE, shell=True)
    _output = _try.communicate()[0].decode("utf-8").strip()

    return _output


if __name__ == "__main__":
    print(uname())
    print(distro())

# 편집: VIM (Ubuntu 18.04)
# 마지막 갱신: 2021년 10월 10일
