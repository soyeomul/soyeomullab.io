# -*- coding: utf-8 -*-

# gitlab.com/soyeomul

REPO = [
    "/hawnoo",
    "/stuff",
    "/__yw__",
    "/debian-www",
    "/weblog",
    "/Gnus",
    "/test",
    "/soyeomul.gitlab.io",
]

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2021년 4월 5일
