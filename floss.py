# -*- coding: utf-8 -*-

# 국내 리눅스 및 오픈소스 사이트 및 프로젝트 목록

ORG = [
    "http://gnome.or.kr",
    "https://debianusers.or.kr",
    "http://korea.gnu.org",
    "https://kldp.org",
    "https://www.ubuntu-kr.org",
    "https://www.fedoralinux.or.kr",
    "https://hamonikr.org",
    "http://no1linux.org",
    "http://www.mozilla.or.kr",
    "https://database.sarang.net",
    "https://www.kr.freebsd.org",
    "https://rust-kr.org",
    "http://www.python.or.kr",
    "https://phpschool.com",
    "http://openstack.or.kr",
    "http://gimp.or.kr",
    "https://opentutorials.org",
]

PROJ = [
    "github.com/libhangul/libhangul (%s)" % ("열린 한글 프로젝트"),
    "github.com/spellcheck-ko/hunspell-dict-ko (%s)" % ("한국어 맞춤법 검사"),
    "github.com/libhangul/ibus-hangul (%s)" % ("리눅스 한글 입력기"),
    "github.com/gureum/gureum (%s)" % ("macOS 한글 입력기"),
    "github.com/Riey/kime (%s)" % ("리눅스 한글 입력기 -- Rust"),
    "github.com/korean-input/issues (%s)" % ("한글 입력에 관한 버그 사례 모음"),
    "github.com/konlpy/konlpy (%s)" % ("한국어 형태소 분석 -- Python"),
    "github.com/libhangul/nabi (%s)" % ("리눅스 한글 입력기 -- X11 전용"),
]

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2021년 3월 16일
