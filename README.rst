::

   # -*- coding: utf-8 -*-

The World is Linux and OpenSource ^^^
-------------------------------------

1. 접속 인터넷 주소 (URI):

   ===> <`https://soyeomul.gitlab.io <https://soyeomul.gitlab.io>`_>
   
2. HTML5 문법 유효성 검사 (HTML5 Validation):

   ===> <`https://validator.w3.org/check?uri=https%3A%2F%2Fsoyeomul.gitlab.io%2F <https://validator.w3.org/check?uri=https%3A%2F%2Fsoyeomul.gitlab.io%2F>`_>

3. GitLab 환경변수 목록 (Predefined variables for GitLab):
   
   ===> <`https://soyeomul.gitlab.io/env.txt <https://soyeomul.gitlab.io/env.txt>`_>
   
::
   
   # 편집: GNU Emacs 27.1 (Ubuntu 18.04)
   # 최초 작성일: 2021년 3월 5일
   # 마지막 갱신: 2021년 4월 5일
